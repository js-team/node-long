Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: long
Upstream-Contact: https://github.com/dcodeIO/long.js/issues
Source: https://github.com/dcodeIO/long.js#readme

Files: *
Copyright: 2024, Daniel Wirtz <dcode@dcode.io>
           2009-2024 The Closure Library Authors
License: Apache-2.0

Files: debian/*
Copyright: 2024, Marco Trevisan (Treviño) <marco@ubuntu.com>
License: Apache-2.0

Files: tests/goog/*.js
Copyright: 2024, Daniel Wirtz <dcode@dcode.io>
           2009-2024 The Closure Library Authors
License: Apache-2.0
Comment: Adapted from https://github.com/google/closure-library/blob/master/closure/goog/math/long.js

License: Apache-2.0
 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at
 .
 http://www.apache.org/licenses/LICENSE-2.0
 .
 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
 .
 On Debian systems, the complete text of the Apache version 2.0 license
 can be found in "/usr/share/common-licenses/Apache-2.0".
